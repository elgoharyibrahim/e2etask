import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HotelsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HotelsProvider {
  url="https://api.myjson.com/bins/tl0bp";
  constructor(public http: HttpClient) {
    console.log('Hello HotelsProvider Provider');
  }
  get() {
    return this.http.get(this.url);
  }
}
