import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HotelListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hotel-list',
  templateUrl: 'hotel-list.html',
})
export class HotelListPage {
  public hotels;
  bool = false;
  boolName = false;
  structure: any = { lower: 0, upper: 200 };
  nameFlag = false;
  priceFlag = false;
  name;
  list;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.hotels = this.navParams.get('hotels');
    this.list = this.hotels;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad HotelListPage');
  }
  reserve() {
    this.bool = !this.bool;
  }
  reserveName() {
    this.boolName = !this.boolName;
  }

  filterName() {
    if (this.name == undefined) {
      this.name = "";
    }
    this.list = this.hotels.filter(s => s.name.toLowerCase().includes(this.name.toLowerCase()));
    this.boolName = false;
  }
  filterPrice() {
    this.list = this.hotels.filter(s => s.price >= this.structure.lower && s.price <= this.structure.upper);
    this.bool = false;
  }
  sortName() {
    this.nameFlag = !this.nameFlag;
    if (this.nameFlag == true) {
      this.list = this.list.sort((a, b) => a.name.localeCompare(b.name));
    }
    else {
      this.list = this.list.sort((a, b) => b.name.localeCompare(a.name));
    }
  }
  sortPrice() {

    this.priceFlag = !this.priceFlag;
    if (this.priceFlag == true) {
      this.list = this.list.sort((a, b) => a.price < b.price ? -1 : 1);
    }
    else {
      this.list = this.list.sort((a, b) => b.price < a.price ? -1 : 1);
    }
  }
}
