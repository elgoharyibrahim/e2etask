import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HotelsProvider} from '../../providers/hotels/hotels';

/**
 * Generated class for the SearchHotelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-hotel',
  templateUrl: 'search-hotel.html',
})
export class SearchHotelPage {
   hotels;
   dateFrom: String = new Date().toISOString().substring(0, 10); 
   dateTo: String = new Date().toISOString().substring(0, 10); 
  constructor(public navCtrl: NavController, public navParams: NavParams,public _hotelsProvider:HotelsProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchHotelPage');
  }
  search(){
    console.log(this.dateFrom);
    console.log(this.dateTo);
    this._hotelsProvider.get().subscribe(
      (data:any)=>{
        console.log(data.hotels);
        this.hotels=data.hotels;
        this.navCtrl.push('HotelListPage',{hotels:this.hotels});
      },(error:Response)=>{
        console.log(error);
      }
    )
  }
}
