import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchHotelPage } from './search-hotel';

@NgModule({
  declarations: [
    SearchHotelPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchHotelPage),
  ],
})
export class SearchHotelPageModule {}
